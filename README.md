# Docker plantUML

Docker image to build UML diagrams based on PlantUML

# CI integration example

```yaml
plant_uml:
  tags: 
    - docker
  image: registry.gitlab.com/d6930/docker-plantuml:latest
  script:
    - java -jar /app/plantuml.jar file.puml
  artifacts:
    name: "$CI_COMMIT_SHORT_SHA.uml"
    paths:
      - file.png
```