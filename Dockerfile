FROM openjdk:19-jdk-alpine3.15

ENV PLANTUML_VERSION 1.2022.2


RUN apk add --no-cache \
    curl \
    ttf-droid \
    ttf-droid-nonlatin \
    graphviz \
    && mkdir app \
    && mkdir tests \
    && curl -L https://sourceforge.net/projects/plantuml/files/plantuml.${PLANTUML_VERSION}.jar/download -o /app/plantuml.jar \